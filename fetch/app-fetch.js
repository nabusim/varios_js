async function getUserAsync() {
    let response = await fetch(`https://randomuser.me/api/`);
    let data = await response.json()
    return data;
}

(async function () {
    try {
        let user = await getUserAsync();
        const userName = user.results[0].name.first;
        const srcImage = user.results[0].picture.medium;
        // UI
        const userImage = document.getElementById('user-img');
        const divContenido = document.getElementById('div-content');
        const userNameText = document.getElementById("user-name");
        //
        userImage.src = srcImage;
        divContenido.style.display = "block";
        userNameText.textContent = userName;
        //
        // console.log(JSON.stringify(user));
        console.log(user);

    } catch (error) {
        error => console.error('Error:', error)
    }
})()