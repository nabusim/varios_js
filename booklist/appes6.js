class Book {
  constructor(title, author, isbn) {
    this.title = title;
    this.author = author;
    this.isbn = isbn;
  }
}

class UI {
  constructor() {
    // Form inputs
    this.title = document.getElementById('title');
    this.author = document.getElementById('author');
    this.isbn = document.getElementById('isbn');

    // Book list
    this.bookList = document.getElementById('book-list');

    // Form
    this.containerForm = document.querySelector('.container');
    this.bookForm = document.querySelector('#book-form');
  }

  addBookToList(book) {
    const list = this.bookList;

    const row = document.createElement('tr');
    row.innerHTML = `
          <td>${book.title}</td>
          <td>${book.author}</td>
          <td>${book.isbn}</td>
          <td><a href='#' class='delete'>X</a></td>
      `;
    list.appendChild(row);
  }

  showAlert(message, className) {
    const div = document.createElement('div');
    div.className = `alert ${className}`;
    div.appendChild(document.createTextNode(message));

    // Insert the alert div
    const container = this.containerForm;
    const form = this.bookForm;
    container.insertBefore(div, form);
    //
    setTimeout(function() {
      document.querySelector('.alert').remove();
    }, 1500);
  }

  clearFields() {
    this.title.value = '';
    this.author.value = '';
    this.isbn.value = '';
  }

  deleteBook(element) {
    element.parentElement.parentElement.remove();
  }
}

// Local Storage class
class Store {
  static addBook(book) {
    const books = Store.getBooks();
    books.push(book);
    localStorage.setItem('books', JSON.stringify(books));
  }

  static getBooks() {
    let books = [];
    if (localStorage.getItem('books') !== null) {
      books = JSON.parse(localStorage.getItem('books'));
    }
    return books;
  }

  static displayBooks() {
    const books = Store.getBooks();
    const ui = new UI();
    books.forEach(function(book) {
      ui.addBookToList(book);
    });
  }

  static removeBook(isbn) {
    const books = Store.getBooks();
    books.forEach(function(book, idx) {
      if (book.isbn === isbn) {
        books.splice(idx, 1);
      }
    });
    localStorage.setItem('books', JSON.stringify(books));
  }
}

//EventListeners
document.addEventListener('DOMContentLoaded', function() {
  Store.displayBooks();
  eventListenters();
});

function eventListenters() {
  document.getElementById('book-form').addEventListener('submit', function(e) {
    e.preventDefault();
    ui = new UI();
    if (
      ui.title.value === '' ||
      ui.author.value === '' ||
      ui.isbn.value === ''
    ) {
      ui.showAlert('Please fill in all fields.', 'error');
    } else {
      const book = new Book(ui.title.value, ui.author.value, ui.isbn.value);
      ui.addBookToList(book);
      // add book to LS
      Store.addBook(book);
      ui.showAlert('Book Added!', 'success');
      ui.clearFields();
    }
  });

  document.getElementById('book-list').addEventListener('click', function(e) {
    e.preventDefault();
    ui = new UI();
    if (e.target.className === 'delete') {
      ui.deleteBook(e.target);
      // remove book from LS
      Store.removeBook(
        e.target.parentElement.previousElementSibling.textContent
      );
      ui.showAlert('Book Removed!', 'success');
    }
  });
}
