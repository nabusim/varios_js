// Book constructor
function Book(title, author, isbn) {
  this.title = title;
  this.author = author;
  this.isbn = isbn;
}

// UI constructor
function UI() {
  // Form inputs
  this.title = document.getElementById('title');
  this.author = document.getElementById('author');
  this.isbn = document.getElementById('isbn');

  // Book list
  this.bookList = document.getElementById('book-list');

  // Form
  this.containerForm = document.querySelector('.container');
  this.bookForm = document.querySelector('#book-form');
}

UI.prototype.addBookToList = function(book) {
  const list = this.bookList;

  const row = document.createElement('tr');
  row.innerHTML = `
        <td>${book.title}</td>
        <td>${book.author}</td>
        <td>${book.isbn}</td>
        <td><a href='#' class='delete'>X</a></td>
    `;
  list.appendChild(row);
};

UI.prototype.showAlert = function(message, className) {
  const div = document.createElement('div');
  div.className = `alert ${className}`;
  div.appendChild(document.createTextNode(message));

  // Insert the alert div
  const container = this.containerForm;
  const form = this.bookForm;
  container.insertBefore(div, form);
  //
  setTimeout(function() {
    document.querySelector('.alert').remove();
  }, 1500);
};

UI.prototype.clearFields = function() {
  this.title.value = '';
  this.author.value = '';
  this.isbn.value = '';
};

UI.prototype.deleteBook = function(element) {
  element.parentElement.parentElement.remove();
};

//EventListeners
document.getElementById('book-form').addEventListener('submit', function(e) {
  e.preventDefault();
  ui = new UI();
  if (ui.title.value === '' || ui.author.value === '' || ui.isbn.value === '') {
    ui.showAlert('Please fill in all fields.', 'error');
  } else {
    const book = new Book(ui.title.value, ui.author.value, ui.isbn.value);
    ui.addBookToList(book);
    ui.showAlert('Book Added!', 'success');
    ui.clearFields();
  }
});

document.getElementById('book-list').addEventListener('click', function(e) {
  e.preventDefault();
  ui = new UI();
  if (e.target.className === 'delete') {
    ui.deleteBook(e.target);
    ui.showAlert('Book Removed!','success')
  }
});
