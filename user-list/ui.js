class UI {
    constructor() {
        this.userList = document.querySelector('.list-group');
        //
        this.api = new API();
    }

    async fillList() {
        try {
            const users = await this.api.getUsers();
            users.forEach(user => {
                const li = document.createElement('li');
                li.className = 'list-group-item';
                const txt = document.createTextNode(`${user.name} - ${user.email}`);
                li.appendChild(txt);
                this.userList.appendChild(li);
            });
        } catch (error) {
            console.log(error);
        }
    }

    selectListRow(element) {
        // let listArray = Array.prototype.slice.call(document.querySelectorAll('.list-group-item'));
        let activeElement = document.querySelector('.active');
        if (activeElement) {
            activeElement.classList.remove("active");
        }
        element.classList.add("active");
    }
}