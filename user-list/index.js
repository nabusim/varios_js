
document.addEventListener('DOMContentLoaded', function () {
    // new ui class instance
    const ui = new UI();
    ui.fillList();

    ui.userList.addEventListener('click', function (event) {
        event.preventDefault();
        const element = event.target;
        ui.selectListRow(element);
    })
});