class UI {
  constructor() {
    this.getJokesBtn = document.querySelector('.get-jokes');
    this.numberJokesInput = document.querySelector('input[type="number"]');
    this.jokesList = document.querySelector('.jokes');
  }

  getJokes() {
    const number = this.numberJokesInput.value;
    const list = this.jokesList;

    const xhr = new XMLHttpRequest();
    xhr.open('GET', `http://api.icndb.com/jokes/random/${number}`, true);

    xhr.onload = function() {
      if (this.status === 200) {
        const response = JSON.parse(this.responseText);
        let output = '';
        if (response.type === 'success') {
          response.value.forEach(function(joke) {
            output += `<li>${joke.joke}</li>`;
          });
        } else {
          output += '<li>Something went wrong</li>';
        }
        list.innerHTML = output;
      }
    };

    xhr.send();
  }
}

// EventListeners
function eventListenters() {
  document.querySelector('.get-jokes').addEventListener('click', function(e) {
    e.preventDefault();
    ui = new UI();
    ui.getJokes();
  });
}

// Load
document.addEventListener('DOMContentLoaded', function() {
  eventListenters();
});
