let fact = document.querySelector('#fact');
let factText = document.querySelector('#fact-text');
let numberInput = document.querySelector('#number-input');

numberInput.addEventListener('input', getFactFetch);

async function getFactFetch() {
  let number = numberInput.value;
  if (number !== '') {
    try {
      let response = await fetch(`http://numbersapi.com/${number}`);
      let data = await response.text();
      if (data != '') {
        fact.style.display = 'block';
        factText.innerText = data;
      }
      // console.log(data);
    } catch (error) {
      console.error(error);
    }
  } else {
    fact.style.display = 'none';
    factText.innerText = '';
  }
}

// OLD OLD
function getFactAjax() {
  let number = numberInput.value;
  let xhr = new XMLHttpRequest();
  let apiUrl = `http://numbersapi.com/${number}`;

  xhr.open('GET', apiUrl);

  xhr.onload = function() {
    if (this.status == 200 && number != '') {
      // console.log(this.responseText);
      fact.style.display = 'block';
      factText.innerText = this.responseText;
    }
  };

  xhr.send();
}
